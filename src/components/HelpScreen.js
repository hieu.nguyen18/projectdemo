import React from "react";
import { View, Text, SafeAreaView, FlatList, Image, StyleSheet } from "react-native"

const DATA = [

    {
        id: 3,
        title: "Bảo Hiểm Sức Khỏe",
        url: require('.././assets/images/image1.png')
    },
    {
        id: 4,
        title: "Bảo Hiểm Tai Nại",
        url: require('.././assets/images/image2.png')
    },
    {
        id: 5,
        title: "Bảo Hiểm TNDS Xe Máy",
        url: require('.././assets/images/image3.png')
    },
    {
        id: 6,
        title: "Bảo Hiểm TNDS Ô tô",
        url: require('.././assets/images/image4.png')
    }, {
        id: 7,
        title: "Bảo Hiểm Đột Quỵ",
        url: require('.././assets/images/image5.png')
    }
]

const RenderItem = ({ item }) => {
    return (
        <View style={styles.container}>
            <View style={styles.contentcontainer} >
                <Image style={styles.imageSize} source={item.url} />
                <Text style={{ marginStart: 20, width: "78%", fontSize: 20 }}>{item.title}</Text>
                <Image style={{ height: 20, width: '7%' }} source={require('.././assets/images/next.png')} />
            </View>
        </View>
    );
}


const HelpScreen = () => {


    return (
        <SafeAreaView style={{ backgroundColor: 'white', height: '100%' }}>
            <FlatList
                data={DATA}
                renderItem={RenderItem}
                keyExtractor={(item) => item.id} />
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },

    contentcontainer: {
        flexDirection: "row",
        marginHorizontal: 15,
        marginTop: 10,
        padding: 9,
        alignItems: "center"
    },
    imageSize: {
        height: 50, 
        width: "15%", 
        resizeMode: "stretch"
    }
})

export default HelpScreen;