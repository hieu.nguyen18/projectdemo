import React from "react";
import { View, Text, FlatList, Button, Image, TouchableOpacity } from "react-native"
import { SafeAreaView } from "react-native-safe-area-context";

const DATA = [
    {
        id: 1,
        title: "Master your data with our component. Check out our worldwide references! The fastest and most complex table, grid, chart or treegrid everywhere! Universal component. Gantt Chart. All",
        url: "https://cdn.24h.com.vn/upload/1-2022/images/2022-03-09/Gia-xang-dau-co-the-cham-moc-30000-dong-lit-nhieu-xang-1646816409-236-width2008height1348.jpeg",
        content: " Nhiều cổ phiếu lớn như GAS, PLX, EIB, HPG, VJC,…đã tăng khá tốt trong phiên chiều nay. Đồng thời, nhiều cổ phiếu liên quan đến hàng hoá cơ bản tăng tích cực củng cố thêm cho sự hồi phục của thị trường. Nhóm cổ phiếu dầu khí ghi nhận sắc tím tại PVC và PVB, trong khi PVD, POS, OIL, PVS... tăng trên 3%. Cổ phiếu phân bón cũng trở lại đường đua khi BFC tăng trần, DCM, DPM, LAS, DGC, VAF, DDV, SFG, PSW tăng tốt.Tâm lý tích cực cũng giúp các cổ phiếu thép (HPG, HSG, NKG, POM, TVN, VGS…) hay than (TC6, TDN,THT, TVD, MDC…) tăng trong phiên hôm nay. Nhóm cổ phiếu cảng biển thu hút dòng tiền, HAH, GMD, PDN, VSC tăng hết biên độ, sắc xanh cũng ghi nhận tại đa số mã của nhóm này.Ngược lại, những bluechips VRE, VHM, MSN, BVH, GVR, POW bị bán mạnh, giảm điểm khiến thị trường chịu lực đè và không thể bứt phá."
    },
    {
        id: 2,
        title: "Master your data with our component. Check out our worldwide references! The fastest and most complex table, grid, chart or treegrid everywhere! Universal component. Gantt Chart. All",
        url: "https://cdn.24h.com.vn/upload/1-2022/images/2022-03-09/Gia-xang-dau-co-the-cham-moc-30000-dong-lit-nhieu-xang-1646816409-236-width2008height1348.jpeg",
        content: " Nhiều cổ phiếu lớn như GAS, PLX, EIB, HPG, VJC,…đã tăng khá tốt trong phiên chiều nay. Đồng thời, nhiều cổ phiếu liên quan đến hàng hoá cơ bản tăng tích cực củng cố thêm cho sự hồi phục của thị trường. Nhóm cổ phiếu dầu khí ghi nhận sắc tím tại PVC và PVB, trong khi PVD, POS, OIL, PVS... tăng trên 3%. Cổ phiếu phân bón cũng trở lại đường đua khi BFC tăng trần, DCM, DPM, LAS, DGC, VAF, DDV, SFG, PSW tăng tốt.Tâm lý tích cực cũng giúp các cổ phiếu thép (HPG, HSG, NKG, POM, TVN, VGS…) hay than (TC6, TDN,THT, TVD, MDC…) tăng trong phiên hôm nay. Nhóm cổ phiếu cảng biển thu hút dòng tiền, HAH, GMD, PDN, VSC tăng hết biên độ, sắc xanh cũng ghi nhận tại đa số mã của nhóm này.Ngược lại, những bluechips VRE, VHM, MSN, BVH, GVR, POW bị bán mạnh, giảm điểm khiến thị trường chịu lực đè và không thể bứt phá."
    },
    {
        id: 3,
        title: "Master your data with our component. Check out our worldwide references! The fastest and most complex table, grid, chart or treegrid everywhere! Universal component. Gantt Chart. All",
        url: "https://cdn.24h.com.vn/upload/1-2022/images/2022-03-09/Gia-xang-dau-co-the-cham-moc-30000-dong-lit-nhieu-xang-1646816409-236-width2008height1348.jpeg",
        content: " Nhiều cổ phiếu lớn như GAS, PLX, EIB, HPG, VJC,…đã tăng khá tốt trong phiên chiều nay. Đồng thời, nhiều cổ phiếu liên quan đến hàng hoá cơ bản tăng tích cực củng cố thêm cho sự hồi phục của thị trường. Nhóm cổ phiếu dầu khí ghi nhận sắc tím tại PVC và PVB, trong khi PVD, POS, OIL, PVS... tăng trên 3%. Cổ phiếu phân bón cũng trở lại đường đua khi BFC tăng trần, DCM, DPM, LAS, DGC, VAF, DDV, SFG, PSW tăng tốt.Tâm lý tích cực cũng giúp các cổ phiếu thép (HPG, HSG, NKG, POM, TVN, VGS…) hay than (TC6, TDN,THT, TVD, MDC…) tăng trong phiên hôm nay. Nhóm cổ phiếu cảng biển thu hút dòng tiền, HAH, GMD, PDN, VSC tăng hết biên độ, sắc xanh cũng ghi nhận tại đa số mã của nhóm này.Ngược lại, những bluechips VRE, VHM, MSN, BVH, GVR, POW bị bán mạnh, giảm điểm khiến thị trường chịu lực đè và không thể bứt phá."
    },
    {
        id: 4,
        title: "Master your data with our component. Check out our worldwide references! The fastest and most complex table, grid, chart or treegrid everywhere! Universal component. Gantt Chart. All",
        url: "https://cdn.24h.com.vn/upload/1-2022/images/2022-03-09/Gia-xang-dau-co-the-cham-moc-30000-dong-lit-nhieu-xang-1646816409-236-width2008height1348.jpeg",
        content: " Nhiều cổ phiếu lớn như GAS, PLX, EIB, HPG, VJC,…đã tăng khá tốt trong phiên chiều nay. Đồng thời, nhiều cổ phiếu liên quan đến hàng hoá cơ bản tăng tích cực củng cố thêm cho sự hồi phục của thị trường. Nhóm cổ phiếu dầu khí ghi nhận sắc tím tại PVC và PVB, trong khi PVD, POS, OIL, PVS... tăng trên 3%. Cổ phiếu phân bón cũng trở lại đường đua khi BFC tăng trần, DCM, DPM, LAS, DGC, VAF, DDV, SFG, PSW tăng tốt.Tâm lý tích cực cũng giúp các cổ phiếu thép (HPG, HSG, NKG, POM, TVN, VGS…) hay than (TC6, TDN,THT, TVD, MDC…) tăng trong phiên hôm nay. Nhóm cổ phiếu cảng biển thu hút dòng tiền, HAH, GMD, PDN, VSC tăng hết biên độ, sắc xanh cũng ghi nhận tại đa số mã của nhóm này.Ngược lại, những bluechips VRE, VHM, MSN, BVH, GVR, POW bị bán mạnh, giảm điểm khiến thị trường chịu lực đè và không thể bứt phá."
    },
    {
        id: 5,
        title: "Master your data with our component. Check out our worldwide references! The fastest and most complex table, grid, chart or treegrid everywhere! Universal component. Gantt Chart. All",
        url: "https://cdn.24h.com.vn/upload/1-2022/images/2022-03-09/Gia-xang-dau-co-the-cham-moc-30000-dong-lit-nhieu-xang-1646816409-236-width2008height1348.jpeg",
        content: " Nhiều cổ phiếu lớn như GAS, PLX, EIB, HPG, VJC,…đã tăng khá tốt trong phiên chiều nay. Đồng thời, nhiều cổ phiếu liên quan đến hàng hoá cơ bản tăng tích cực củng cố thêm cho sự hồi phục của thị trường. Nhóm cổ phiếu dầu khí ghi nhận sắc tím tại PVC và PVB, trong khi PVD, POS, OIL, PVS... tăng trên 3%. Cổ phiếu phân bón cũng trở lại đường đua khi BFC tăng trần, DCM, DPM, LAS, DGC, VAF, DDV, SFG, PSW tăng tốt.Tâm lý tích cực cũng giúp các cổ phiếu thép (HPG, HSG, NKG, POM, TVN, VGS…) hay than (TC6, TDN,THT, TVD, MDC…) tăng trong phiên hôm nay. Nhóm cổ phiếu cảng biển thu hút dòng tiền, HAH, GMD, PDN, VSC tăng hết biên độ, sắc xanh cũng ghi nhận tại đa số mã của nhóm này.Ngược lại, những bluechips VRE, VHM, MSN, BVH, GVR, POW bị bán mạnh, giảm điểm khiến thị trường chịu lực đè và không thể bứt phá."
    },
    {
        id: 6,
        title: "Master your data with our component. Check out our worldwide references! The fastest and most complex table, grid, chart or treegrid everywhere! Universal component. Gantt Chart. All",
        url: "https://cdn.24h.com.vn/upload/1-2022/images/2022-03-09/Gia-xang-dau-co-the-cham-moc-30000-dong-lit-nhieu-xang-1646816409-236-width2008height1348.jpeg",
        content: " Nhiều cổ phiếu lớn như GAS, PLX, EIB, HPG, VJC,…đã tăng khá tốt trong phiên chiều nay. Đồng thời, nhiều cổ phiếu liên quan đến hàng hoá cơ bản tăng tích cực củng cố thêm cho sự hồi phục của thị trường. Nhóm cổ phiếu dầu khí ghi nhận sắc tím tại PVC và PVB, trong khi PVD, POS, OIL, PVS... tăng trên 3%. Cổ phiếu phân bón cũng trở lại đường đua khi BFC tăng trần, DCM, DPM, LAS, DGC, VAF, DDV, SFG, PSW tăng tốt.Tâm lý tích cực cũng giúp các cổ phiếu thép (HPG, HSG, NKG, POM, TVN, VGS…) hay than (TC6, TDN,THT, TVD, MDC…) tăng trong phiên hôm nay. Nhóm cổ phiếu cảng biển thu hút dòng tiền, HAH, GMD, PDN, VSC tăng hết biên độ, sắc xanh cũng ghi nhận tại đa số mã của nhóm này.Ngược lại, những bluechips VRE, VHM, MSN, BVH, GVR, POW bị bán mạnh, giảm điểm khiến thị trường chịu lực đè và không thể bứt phá."
    },
    {
        id: 7,
        title: "Master your data with our component. Check out our worldwide references! The fastest and most complex table, grid, chart or treegrid everywhere! Universal component. Gantt Chart. All",
        url: "https://cdn.24h.com.vn/upload/1-2022/images/2022-03-09/Gia-xang-dau-co-the-cham-moc-30000-dong-lit-nhieu-xang-1646816409-236-width2008height1348.jpeg",
        content: " Nhiều cổ phiếu lớn như GAS, PLX, EIB, HPG, VJC,…đã tăng khá tốt trong phiên chiều nay. Đồng thời, nhiều cổ phiếu liên quan đến hàng hoá cơ bản tăng tích cực củng cố thêm cho sự hồi phục của thị trường. Nhóm cổ phiếu dầu khí ghi nhận sắc tím tại PVC và PVB, trong khi PVD, POS, OIL, PVS... tăng trên 3%. Cổ phiếu phân bón cũng trở lại đường đua khi BFC tăng trần, DCM, DPM, LAS, DGC, VAF, DDV, SFG, PSW tăng tốt.Tâm lý tích cực cũng giúp các cổ phiếu thép (HPG, HSG, NKG, POM, TVN, VGS…) hay than (TC6, TDN,THT, TVD, MDC…) tăng trong phiên hôm nay. Nhóm cổ phiếu cảng biển thu hút dòng tiền, HAH, GMD, PDN, VSC tăng hết biên độ, sắc xanh cũng ghi nhận tại đa số mã của nhóm này.Ngược lại, những bluechips VRE, VHM, MSN, BVH, GVR, POW bị bán mạnh, giảm điểm khiến thị trường chịu lực đè và không thể bứt phá."

    },
    {
        id: 8,
        title: "Master your data with our component. Check out our worldwide references! The fastest and most complex table, grid, chart or treegrid everywhere! Universal component. Gantt Chart. All",
        url: "https://cdn.24h.com.vn/upload/1-2022/images/2022-03-09/Gia-xang-dau-co-the-cham-moc-30000-dong-lit-nhieu-xang-1646816409-236-width2008height1348.jpeg",
        content: " Nhiều cổ phiếu lớn như GAS, PLX, EIB, HPG, VJC,…đã tăng khá tốt trong phiên chiều nay. Đồng thời, nhiều cổ phiếu liên quan đến hàng hoá cơ bản tăng tích cực củng cố thêm cho sự hồi phục của thị trường. Nhóm cổ phiếu dầu khí ghi nhận sắc tím tại PVC và PVB, trong khi PVD, POS, OIL, PVS... tăng trên 3%. Cổ phiếu phân bón cũng trở lại đường đua khi BFC tăng trần, DCM, DPM, LAS, DGC, VAF, DDV, SFG, PSW tăng tốt.Tâm lý tích cực cũng giúp các cổ phiếu thép (HPG, HSG, NKG, POM, TVN, VGS…) hay than (TC6, TDN,THT, TVD, MDC…) tăng trong phiên hôm nay. Nhóm cổ phiếu cảng biển thu hút dòng tiền, HAH, GMD, PDN, VSC tăng hết biên độ, sắc xanh cũng ghi nhận tại đa số mã của nhóm này.Ngược lại, những bluechips VRE, VHM, MSN, BVH, GVR, POW bị bán mạnh, giảm điểm khiến thị trường chịu lực đè và không thể bứt phá."
    },
    {
        id: 9,
        title: "Master your data with our component. Check out our worldwide references! The fastest and most complex table, grid, chart or treegrid everywhere! Universal component. Gantt Chart. All",
        url: 'https://cdn.24h.com.vn/upload/1-2022/images/2022-03-09/Gia-xang-dau-co-the-cham-moc-30000-dong-lit-nhieu-xang-1646816409-236-width2008height1348.jpeg',
        content: " Nhiều cổ phiếu lớn như GAS, PLX, EIB, HPG, VJC,…đã tăng khá tốt trong phiên chiều nay. Đồng thời, nhiều cổ phiếu liên quan đến hàng hoá cơ bản tăng tích cực củng cố thêm cho sự hồi phục của thị trường. Nhóm cổ phiếu dầu khí ghi nhận sắc tím tại PVC và PVB, trong khi PVD, POS, OIL, PVS... tăng trên 3%. Cổ phiếu phân bón cũng trở lại đường đua khi BFC tăng trần, DCM, DPM, LAS, DGC, VAF, DDV, SFG, PSW tăng tốt.Tâm lý tích cực cũng giúp các cổ phiếu thép (HPG, HSG, NKG, POM, TVN, VGS…) hay than (TC6, TDN,THT, TVD, MDC…) tăng trong phiên hôm nay. Nhóm cổ phiếu cảng biển thu hút dòng tiền, HAH, GMD, PDN, VSC tăng hết biên độ, sắc xanh cũng ghi nhận tại đa số mã của nhóm này.Ngược lại, những bluechips VRE, VHM, MSN, BVH, GVR, POW bị bán mạnh, giảm điểm khiến thị trường chịu lực đè và không thể bứt phá."
    }
]



const HomeScreen = ({ navigation }) => {

    const renderItem = ({ item }) => {
        return (
            <View style={{ height: 100, backgroundColor: 'white' }}>
                <TouchableOpacity
                    style={{ flexDirection: "row", margin: 10 }}
                    onPress={() => navigation.navigate("Detail", { items: item })} >
                    <Image style={{ height: 70, width: "20%", resizeMode: "stretch" }} source={{ uri: item.url }} />
                    <Text style={{ marginStart: 10, width: "80%" }}>{item.title}</Text>
                </TouchableOpacity>
            </View>
        );
    }

    return (
        <SafeAreaView>
            <FlatList
                data={DATA}
                renderItem={renderItem}
                keyExtractor={(item) => item.id} />
        </SafeAreaView>
    )
}

export default HomeScreen;