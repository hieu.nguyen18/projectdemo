import React from "react";
import { View, Text, Image } from "react-native"
import { SafeAreaView } from "react-native-safe-area-context";

const HomeDetailScreen = ({ navigation, route }) => {
    return (
        <SafeAreaView>
            <View style={{ backgroundColor: 'white', height: '100%', width: '100%', padding: 10 }}>
                <Text> {route.params.items.title} </Text>
                <Image style={{ height: 220, width: "100%", resizeMode: "stretch", margin: 10 }} source={{ uri: route.params.items.url }} />
                <Text> {route.params.items.content} </Text>
            </View>
        </SafeAreaView>
    )
}

export default HomeDetailScreen;