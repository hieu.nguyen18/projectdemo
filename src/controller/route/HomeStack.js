import React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";

import HomeScreen from "../../components/HomeScreen"
import HomeDetailScreen from "../../components/HomeDetailScreen";
import HelpScreen from "../../components/HelpScreen";
import NewsScreen from "../../components/NewsScreen";
import NotifiScreen from "../../components/NofitiScreen";

const HomeStack = createNativeStackNavigator();
const HelpStack = createNativeStackNavigator();
const NewsStack = createNativeStackNavigator();
const NotifiStack = createNativeStackNavigator();

const HomeStackScreen = () => {
    return(
      <HomeStack.Navigator initialRouteName="Home" >
          <HomeStack.Screen name="Home" component={HomeScreen}  options={{title: "Trang chủ"}}/>
          <HomeStack.Screen name="Detail" component={HomeDetailScreen} options={{title: "Chi tiết"}} />
      </HomeStack.Navigator>
    )
}

 export const HelpStackScreen = () => {
  return(
    <HelpStack.Navigator >
        <HelpStack.Screen name="Help" component={HelpScreen} options={{title: "Hỗ trợ"}} />
    </HelpStack.Navigator>
  )
}

export const NewsStackScreen = () => {
  return(
    <NewsStack.Navigator >
        <NewsStack.Screen name="News" component={NewsScreen} options={{title: "Tin tức"}} />
    </NewsStack.Navigator>
  )
}

export const NotifiStackScreen = () => {
  return(
    <NotifiStack.Navigator >
        <NotifiStack.Screen name="Nofiti" component={NotifiScreen} options={{title: "Thông báo"}}/>
    </NotifiStack.Navigator>
  )
}

export default HomeStackScreen;