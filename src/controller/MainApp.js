import React from "react";
import { View, Text } from "react-native"
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

import HomeStack from "./route/HomeStack";
import { HelpStackScreen } from "./route/HomeStack";
import { NewsStackScreen } from "./route/HomeStack";
import { NotifiStackScreen } from "./route/HomeStack";


const Tab = createBottomTabNavigator();

const App = () => {
    return (

        <NavigationContainer>
            <Tab.Navigator initialRouteName="Home"screenOptions={{headerShown:false}}>
                <Tab.Screen name="Home" component={HomeStack} 
                    options={{title: "Trang chủ"}} />
                <Tab.Screen name="Help" component={HelpStackScreen} options={{title: "Hỗ trợ"}}/>
                <Tab.Screen name="News" component={NewsStackScreen} options={{title: "Tin tức"}}/>
                <Tab.Screen name="Nofiti" component={NotifiStackScreen} options={{title: "Thông báo"}}/>
            </Tab.Navigator>
        </NavigationContainer>
    )
}

export default App;